import * as Phaser from "phaser";
import { AbstractAssetLoader } from "phaser3-class-preloader";
import { _ } from "Assets/_";

export class AssetLoader extends AbstractAssetLoader {

    preload() {
        this.load
            .multiatlas(_.Atlas.DEFAULT, require("Assets/images/default/atlas.json").json)
            .spritesheet(_.Atlas.PLAYER_IDLE, require("Assets/images/default/player-idle.png"), {
                frameWidth: 50,
                frameHeight: 37
            })
            .image(_.Atlas.ENEMY1, require("Assets/images/default/ship3.png"))
            .spritesheet(_.Atlas.PLAYER_ATTACK, require("Assets/images/default/player-attack.png"), {
                frameWidth: 50,
                frameHeight: 37
            })
            .spritesheet(_.Atlas.BULLET, require("Assets/images/spritesheet/fireball.png"), {
                frameWidth: 64,
                frameHeight: 64
            })
            .spritesheet(_.Atlas.PLAYER_HURT, require("Assets/images/default/player-hurt.png"), {
                frameWidth: 50,
                frameHeight: 37
            })
            .spritesheet(_.Atlas.PLAYER_DIE, require("Assets/images/default/player-die.png"), {
                frameWidth: 50,
                frameHeight: 37
            })
            .audio(_.BGM.MENU_BGM, require("Assets/bgms/menu-bgm.mp3"))
            .audio(_.BGM.GAME_BGM, require("Assets/bgms/game-bgm.mp3"))
            .audio(_.SFX.ATTACK_SFX, require("Assets/sfxs/attack-sfx.mp3"))
            .audio(_.SFX.DIE_SFX, require("Assets/sfxs/die-sfx.mp3"))
            .audio(_.SFX.EXPLOSION_SFX, require("Assets/sfxs/explosion-sfx.mp3"))
            .audio(_.SFX.HURT_SFX, require("Assets/sfxs/hurt-sfx.mp3"))
            ;
    }


}