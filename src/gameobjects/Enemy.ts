import * as Phaser from "phaser";
import { _ } from "Assets/_";

export class Enemy extends Phaser.GameObjects.Sprite{
    constructor(scene: Phaser.Scene, texture: string){
        let x = 150;
        let y = 150;

        super(scene, x, y, texture);
        scene.add.existing(this);
        scene.physics.world.enableBody(this);
        ///@ts-ignore
        scene.enemies.add(this);

        this.setScale(2);
    }

    FindPlayer(player: Phaser.GameObjects.Sprite){
        this.rotation = Phaser.Math.Angle.Between(this.x, this.y, player.x, player.y);
    }

    ResetRandomPos(player: Phaser.GameObjects.Sprite, moveSpeed: integer){
        
        let whereToSpawn = Math.floor(Math.random() * 4);

        let randWidth = Math.floor(Math.random() * this.scene.game.renderer.width);
        let randHeight = Math.floor(Math.random() * this.scene.game.renderer.height);

        switch(whereToSpawn){
            case 0:
                this.setPosition(randWidth, 0);
                this.scene.physics.moveToObject(this, player, moveSpeed);
                break;
            case 1:
                this.setPosition(0, randHeight);
                this.scene.physics.moveToObject(this, player, moveSpeed);
                break;
            case 2:
                this.setPosition(this.scene.game.renderer.width, randHeight);
                this.scene.physics.moveToObject(this, player, moveSpeed);
                break;
            case 3:
                this.setPosition(randWidth, this.scene.game.renderer.height);
                this.scene.physics.moveToObject(this, player, moveSpeed);
                break;
            default:
                break;
        }

        this.FindPlayer(player);
    }
}