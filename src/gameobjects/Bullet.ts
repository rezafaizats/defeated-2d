import * as Phaser from "phaser";
import { _ } from "Assets/_";

export class Bullet extends Phaser.GameObjects.Sprite{
    
    constructor(scene: Phaser.Scene, player: Phaser.GameObjects.Sprite, xPointer: integer, yPointer: integer){
        
        let x = player.x;
        let y = player.y;

        super(scene, x, y, _.Atlas.BULLET);
        scene.add.existing(this);
        scene.physics.world.enableBody(this);

        this.play(_.Animation.BULLET_ANIM_KEY.BULLET_ANIM);
        this.rotation = Phaser.Math.Angle.Between(this.x + this.width, this.y + this.height, xPointer, yPointer);
        this.setScale(1.35);

        //@ts-ignore
        scene.bullets.add(this);
        scene.physics.moveTo(this, xPointer, yPointer, 250);
    }

    update(){
        if(this.y < 0 || this.x < 0 || this.x > 820 || this.y > 1024){
            this.destroy();
        }
    }

}