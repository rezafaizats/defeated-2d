import * as Phaser from "phaser";
import { _ } from "Assets/_";

export class Player extends Phaser.GameObjects.Sprite{
    
    constructor(scene: Phaser.Scene){

        let x = scene.game.renderer.width / 2;
        let y = scene.game.renderer.height / 2;

        super(scene, x, y, _.Atlas.PLAYER_IDLE);
        
        scene.add.existing(this);
        scene.physics.world.enableBody(this);
        (this.body as Phaser.Physics.Arcade.Body).immovable = true;

        this.setScale(2.5);
        this.play(_.Animation.PLAYER_ANIM_KEY.PLAYER_IDLE_ANIM);
    }

    PlayAttackAnim(){

        let attack_sfx = this.scene.sound.add(_.SFX.ATTACK_SFX);
        attack_sfx.play();

        this.play(_.Animation.PLAYER_ANIM_KEY.PLAYER_ATTACK_ANIM);
        this.once('animationcomplete', () => {
            attack_sfx.stop();
            this.play(_.Animation.PLAYER_ANIM_KEY.PLAYER_IDLE_ANIM);
        });
    }

    PlayHurtAnim(){

        let hurt_sfx = this.scene.sound.add(_.SFX.HURT_SFX);
        ///@ts-ignore
        if(this.scene.lives > 0){
            hurt_sfx.play();
        }

        this.play(_.Animation.PLAYER_ANIM_KEY.PLAYER_HURT_ANIM);
        this.once('animationcomplete', () => {
            this.play(_.Animation.PLAYER_ANIM_KEY.PLAYER_IDLE_ANIM);
        });
    }

    PlayDieAnim(){
        
        let die_sfx = this.scene.sound.add(_.SFX.DIE_SFX);
        die_sfx.play();

        this.play(_.Animation.PLAYER_ANIM_KEY.PLAYER_DIE_ANIM);
        this.once('animationcomplete', () => {
            die_sfx.stop();
            this.scene.scene.start(_.Scene.TITLE_GAME);
        });
    }

}