import * as Phaser from "phaser";
import { _ } from "Assets/_";
import { Bullet } from "../gameobjects/Bullet"
import { Player } from "../gameobjects/Player"
import { Enemy } from "../gameobjects/Enemy"

export class GameScene extends Phaser.Scene{

    enemy!: Enemy
    player!: Player
    xPointer!: integer
    yPointer!: integer
    score!: integer
    lives!: integer
    scoreLabel!: Phaser.GameObjects.Text
    livesLabel!: Phaser.GameObjects.Text
    bullets!: Phaser.GameObjects.Group
    enemies!: Phaser.GameObjects.Group
    music!: Phaser.Sound.BaseSound
    moveSpeed = 350;

    constructor(){
        super(_.Scene.GAME);
    }

    create(){

        this.physics.world.setBounds(0, 0, this.game.renderer.width, this.game.renderer.height);
        console.log(this.game.renderer.width + "X" + this.game.renderer.height);
        
        this.bullets = this.physics.add.group();
        this.bullets.runChildUpdate = true;
        this.enemies = this.physics.add.group();

        this.add.image(this.game.renderer.width / 2, this.game.renderer.height / 2, _.Image.BG_IMAGE.texture, _.Image.BG_IMAGE.frame).setDepth(0);
        this.player = new Player(this);
        this.enemy = new Enemy(this, _.Image.ENEMY1.texture);
        this.player.setDepth(1);
        this.enemy.setDepth(1);

        this.music = this.sound.add(_.BGM.GAME_BGM);
        let music_config = {
            volume: 0.5,
            loop: true
        }
        this.music.play(music_config);

        this.score = 0;
        this.scoreLabel = this.add.text(this.game.renderer.width / 8, 5, "SCORE : " + this.score, _.TextStyle.UI);
        this.lives = 3;
        this.livesLabel = this.add.text(this.game.renderer.width - (this.game.renderer.width / 4), 5, "LIVES : " + this.lives, _.TextStyle.UI);

        this.physics.add.overlap(this.enemies, this.player, () => this.player.PlayHurtAnim());
        this.physics.add.overlap(this.enemies, this.player, () => this.HurtPlayer());
        this.physics.add.overlap(this.enemies, this.player, () => this.enemy.ResetRandomPos(this.player, this.moveSpeed));
        this.physics.add.overlap(this.bullets, this.enemies, () => {
            this.addScore()
            this.enemy.ResetRandomPos(this.player, this.moveSpeed)
            this.cameras.main.shake();
        })
        this.physics.add.overlap(this.bullets, this.enemies, function(bullet, enemy){
            bullet.destroy();
        })

        this.input.on(Phaser.Input.Events.POINTER_DOWN, () => this.spawnBullet(), this); 
    }

    update(time: number, delta: number){
        this.bullets.preUpdate(time, delta);
    }

    spawnBullet(){
        this.player.PlayAttackAnim();

        this.xPointer = this.game.input.activePointer.x;
        this.yPointer = this.game.input.activePointer.y;
        
        if(this.xPointer <= this.game.renderer.width / 2){
            this.player.flipX = true;
        }else{
            this.player.flipX = false;
        }

        let fireball = new Bullet(this, this.player, this.xPointer, this.yPointer);
        fireball.setDepth(1);
    }

    addScore(){        
        let explosion_sfx = this.sound.add(_.SFX.EXPLOSION_SFX);
        explosion_sfx.play();

        this.score += 10;
        this.scoreLabel.text = "SCORE : " + this.score;
    }

    HurtPlayer(){
        this.lives--;
        if(this.lives < 0){
            this.lives = 0;
            this.music.stop();
            this.player.PlayDieAnim();
        }
        this.livesLabel.text = "LIVES : " + this.lives;
    }

}