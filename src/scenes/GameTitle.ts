import * as Phaser from "phaser";
import { _ } from "Assets/_";

export class GameTitle extends Phaser.Scene{

    menu_bgm!: Phaser.Sound.BaseSound
    
    constructor(){
        super(_.Scene.TITLE_GAME);
    }

    create(){
        
        let playButton = this.add.image(this.viewport.center.x, this.viewport.center.y + 150, _.Image.PLAY_BUTTON.texture, _.Image.PLAY_BUTTON.frame).setDepth(1);
        this.add.image(this.viewport.center.x, this.viewport.center.y - 200, _.Image.TITLE_IMAGE.texture, _.Image.TITLE_IMAGE.frame).setDepth(1);
        this.add.image(this.viewport.center.x, this.viewport.center.y, _.Image.BG_IMAGE.texture, _.Image.BG_IMAGE.frame).setDepth(0);        
        
        this.anims.create({
            key: _.Animation.PLAYER_ANIM_KEY.PLAYER_IDLE_ANIM,
            frames: this.anims.generateFrameNames(_.Atlas.PLAYER_IDLE),
            frameRate: 10,
            repeat: -1
            });
        this.anims.create({
            key: _.Animation.PLAYER_ANIM_KEY.PLAYER_ATTACK_ANIM,
            frames: this.anims.generateFrameNames(_.Atlas.PLAYER_ATTACK),
            frameRate: 24,
            repeat: 0
            });
        this.anims.create({
            key: _.Animation.PLAYER_ANIM_KEY.PLAYER_HURT_ANIM,
            frames: this.anims.generateFrameNames(_.Atlas.PLAYER_HURT),
            frameRate: 18,
            repeat: 0
            });
        this.anims.create({
            key: _.Animation.BULLET_ANIM_KEY.BULLET_ANIM,
            frames: this.anims.generateFrameNames(_.Atlas.BULLET),
            frameRate: 20,
            repeat: 0
            });
        this.anims.create({
            key: _.Animation.PLAYER_ANIM_KEY.PLAYER_DIE_ANIM,
            frames: this.anims.generateFrameNames(_.Atlas.PLAYER_DIE),
            frameRate: 18,
            repeat: 0
            });

        this.menu_bgm = this.sound.add(_.BGM.MENU_BGM);
        let menu_config = {
            volume: 0.5,
            loop: true
        }
        this.menu_bgm.play(menu_config);

        playButton.setInteractive();
        this.input.on("gameobjectdown", () => this.PlayGame());
    }

    PlayGame(){
        this.menu_bgm.stop();
        this.scene.start(_.Scene.GAME);
    }

}