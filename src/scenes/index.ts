import { LoadingScene } from "./LoadingScene";
import { TitleScene } from "./TitleScene";
import { SampleScene } from "./SampleScene";
import { GameScene } from "./GameScene";
import { GameTitle } from "./GameTitle";

const dialogs: (new (...args: any[]) => Phaser.Scene)[] = [

];

const popups: (new (...args: any[]) => Phaser.Scene)[] = [

];

const scenes: (new (...args: any[]) => Phaser.Scene)[] = [
    LoadingScene,
    TitleScene,
    SampleScene,    
    GameTitle,
    GameScene,
]

export const Scenes: (new (...args: any[]) => Phaser.Scene)[] = scenes.concat(dialogs, popups);
