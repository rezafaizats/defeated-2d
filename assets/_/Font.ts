import "Assets/fonts/Cairo-Regular.ttf";
import "Assets/fonts/RobotoSlab-Regular.ttf"

export enum Font {

    DEFAULT = "Cairo",
    TITLE = "Cairo",
    DESCRIPTION = "Cairo",
    BUTTON = "Cairo",
    UI_FONT = "Roboto Slab",

}