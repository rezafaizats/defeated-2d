export enum Atlas {
    DEFAULT = "default",
    PLAYER_ATTACK = "player-attack",
    PLAYER_IDLE = "player-idle",
    PLAYER_HURT = "player-hurt",
    PLAYER_DIE = "player-die",
    ENEMY1 = "ship3",
    BULLET = "fireball"
}

export namespace Atlas.Frame {

    export enum DEFAULT {
        MONEY = "money",
        GAMEPAD = "gamepad",
        BUTTON_UP = "button-up",
        BG_IMAGE = "bg_image",
        TITLE_IMAGE = "title_image",
        PLAY_BUTTON = "play_button",
        PLAYER_ATTACK = "player-attack",
        PLAYER_IDLE = "player-idle",
        PLAYER_HURT = "player-hurt",
        PLAYER_DIE = "player-die",
        ENEMY1 = "ship3",
        BULLET = "fireball"
    }

}