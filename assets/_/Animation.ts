export namespace Animation {    

    export enum PLAYER_ANIM_KEY {
        PLAYER_ATTACK_ANIM = "player-attack",
        PLAYER_HURT_ANIM = "player-hurt",
        PLAYER_IDLE_ANIM = "player-idle",
        PLAYER_DIE_ANIM = "player-die",
    }

    export enum BULLET_ANIM_KEY {
        BULLET_ANIM = "fireball"
    }

}