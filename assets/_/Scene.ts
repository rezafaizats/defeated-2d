export enum Scene {
    LOADING = "Scene.Loading",
    TITLE = "Scene.Title",
    SAMPLE = "Scene.Sample",
    GAME = "Scene.Game",
    TITLE_GAME = "Scene.GameTitle",
}

export enum Dialog {
    
}

export enum Popup {

}