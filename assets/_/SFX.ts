export enum SFX {

    ATTACK_SFX = "attack-sfx",
    DIE_SFX = "die-sfx",
    EXPLOSION_SFX = "explosion-sfx",
    HURT_SFX = "hurt-sfx"

}