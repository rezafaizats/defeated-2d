import { Asset } from "Core/Asset"
import { Atlas } from "./Atlas";

export namespace Image {

    export const MONEY:Asset.Image = {
        texture: Atlas.DEFAULT,
        frame: Atlas.Frame.DEFAULT.MONEY
    }

    export const GAMEPAD: Asset.Image = {
        texture: Atlas.DEFAULT,
        frame: Atlas.Frame.DEFAULT.GAMEPAD
    }

    export const BUTTON_UP: Asset.Image = {
        texture: Atlas.DEFAULT,
        frame: Atlas.Frame.DEFAULT.BUTTON_UP,
        ninePatch: {
            top: 10,
            right: 10,
            bottom: 20
        }
    }

    export const BG_IMAGE: Asset.Image = {
        texture: Atlas.DEFAULT,
        frame: Atlas.Frame.DEFAULT.BG_IMAGE
    }

    export const TITLE_IMAGE: Asset.Image = {
        texture: Atlas.DEFAULT,
        frame: Atlas.Frame.DEFAULT.TITLE_IMAGE
    }

    export const PLAY_BUTTON: Asset.Image = {
        texture: Atlas.DEFAULT,
        frame: Atlas.Frame.DEFAULT.PLAY_BUTTON
    }

    export const PLAYER_IDLE: Asset.Image = {
        texture: Atlas.PLAYER_IDLE,
        frame: Atlas.Frame.DEFAULT.PLAYER_IDLE
    }

    export const PLAYER_ATTACK: Asset.Image = {
        texture: Atlas.PLAYER_ATTACK,
        frame: Atlas.Frame.DEFAULT.PLAYER_ATTACK
    }

    export const PLAYER_DIE: Asset.Image = {
        texture: Atlas.PLAYER_DIE,
        frame: Atlas.Frame.DEFAULT.PLAYER_DIE
    }

    export const ENEMY1: Asset.Image = {
        texture: Atlas.ENEMY1,
        frame: Atlas.Frame.DEFAULT.ENEMY1
    }

}